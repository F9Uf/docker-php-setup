# Setup Docker mount directory for Win7

## Work for?
  - Docker toolbox
  - Docker version 18.03.0 ++

## Mount D: drive
- Use this command ```$docker-machine stop``` on terminal for stop vm.
- Open VirsualBox and go to **default** machine.
- On **default** machine > setting > shared folder > click + icon to add **New shared folder** and select folder path to **drive D:** , set folder's name **'d'**.
- Copy **attach-drive-d.sh script** to Docker Toolbox folder (C:\Program Files\Docker Toolbox)
- Run docker-toolbox from shortcut **Docker Quickstart Terminal** or ```$docker-machine start```



### attach-drive-d.sh file:
```
#!/bin/bash

if [ "${VM}" == "default" ]; then
  "${DOCKER_MACHINE}" ssh "${VM}" "sudo mkdir -p /d/"
  "${DOCKER_MACHINE}" ssh "${VM}" "sudo mount -t vboxsf -o defaults,uid=\$(id -u docker),gid=\$(id -g docker) d /d/"
fi
```

# How to access
You can open your browser and go to http://192.168.99.100 on address bar.